﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{


    public enum GameState
    {
        Menu,
        Play,
        GameOver
    }

    public GameState _GameState;

    public int Score;
    public GameObject[] Panels;
    public GameObject[] items;

    public float seconds = 30;
    public float miliseconds = 0;

    void Awake()
    {
        _GameState = GameState.Menu;
        ReturnMenu();
    }


    void Update()
    {

        /** seconds & milliseconds Timer **/
        if (_GameState.Equals(GameState.Play))
        {
            if (miliseconds <= 0)
            {
                seconds--;
                miliseconds = 100;
            }
            else if (miliseconds >= 0)
            {
                miliseconds--;
            }

            miliseconds -= Time.deltaTime * 100;

            if (seconds <= 0)
            {
                _GameState = GameState.GameOver;
                Panels[2].gameObject.SetActive(true);
                gameObject.GetComponent<Swipe>().enabled = false;
                Face.Instance.gameObject.SetActive(false);
                seconds = 0;
                miliseconds = 0;
            }
        }
        /** seconds & milliseconds Timer **/
    }

    public void AddScore()
    {
        Score++;
    }

    public void Play()
    {

        Score = 0;
        seconds = 30;
        miliseconds = 0;
        gameObject.GetComponent<Swipe>().enabled = true;

        // Re-position the face
        Face.Instance.gameObject.SetActive(true);
        Face.Instance.transform.position = new Vector3(0, 200, 0);

        // IF Event called in the Menu Panel
        if (_GameState.Equals(GameState.Menu))
        {
            Panels[0].gameObject.SetActive(false);
            items[0].gameObject.SetActive(true);
            items[1].gameObject.SetActive(true);
        }

        else
        {
            Panels[2].gameObject.SetActive(false);
        }
        
        // Set GameState to Play
        _GameState = GameState.Play;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ReturnMenu()
    {
        // IF Event called in the Menu Panel
        if (_GameState.Equals(GameState.Menu))
        {
            Panels[1].gameObject.SetActive(false);
            Panels[0].gameObject.SetActive(true);
        }

        // IF Event called in the GameOver Panel
        else if (_GameState.Equals(GameState.GameOver))
        {
            items[0].gameObject.SetActive(false);
            items[1].gameObject.SetActive(false);

            Panels[2].gameObject.SetActive(false);
            Panels[0].gameObject.SetActive(true);
        }

        // Set GameState to Menu
        _GameState = GameState.Menu;


    }

    public void Credits()
    {
        Panels[0].gameObject.SetActive(false);
        Panels[1].gameObject.SetActive(true);
    }
}
