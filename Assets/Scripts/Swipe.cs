﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Swipe : MonoBehaviour
{


    private readonly Vector2 mXAxis = new Vector2(1, 0);
    private readonly Vector2 mYAxis = new Vector2(0, 1);



    // The angle range for detecting swipe
    private const float mAngleRange = 30;

    // To recognize as swipe user should at lease swipe for this many pixels
    private const float mMinSwipeDist = 0.5f;

    // To recognize as a swipe the velocity of the swipe
    // should be at least mMinVelocity
    // Reduce or increase to control the swipe speed
    private const float mMinVelocity = 100.0f;

    private Vector2 mStartPosition;
    private float mSwipeStartTime;

    // SoundFx
    public List<AudioClip> slapSounds;
    private AudioSource source;


    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        // Mouse button down, possible chance for a swipe
        if (Input.GetMouseButtonDown(0))
        {


            // Record start time and position
            mStartPosition = new Vector2(Input.mousePosition.x,
                                         Input.mousePosition.y);
            mSwipeStartTime = Time.time;


        }

        // Mouse button up, possible chance for a swipe
        if (Input.GetMouseButtonUp(0))
        {
            Face.Instance.transform.position = new Vector3(0, 0, 0);
            Face.Instance.transform.localScale = new Vector3(1, 1, 0);
            float deltaTime = Time.time - mSwipeStartTime;

            Vector2 endPosition = new Vector2(Input.mousePosition.x,
                                               Input.mousePosition.y);
            Vector2 swipeVector = endPosition - mStartPosition;

            float velocity = swipeVector.magnitude / deltaTime;

            if (velocity > mMinVelocity &&
                swipeVector.magnitude > mMinSwipeDist)
            {
                // if the swipe has enough velocity and enough distance

                swipeVector.Normalize();

                float angleOfSwipe = Vector2.Dot(swipeVector, mXAxis);
                angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;

                // Detect left and right swipe
                if (angleOfSwipe < mAngleRange)
                {
                    OnSwipeRight();
                }
                else if ((180.0f - angleOfSwipe) < mAngleRange)
                {
                    OnSwipeLeft();
                }
                else
                {
                    // Detect top and bottom swipe
                    angleOfSwipe = Vector2.Dot(swipeVector, mYAxis);
                    angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;
                }
            }
        }
    }


    private void OnSwipeLeft()
    {

        GameManager.Instance.AddScore();
        source.PlayOneShot(slapSounds[Random.Range(0, 3)]);

        if (GameManager.Instance.Score < 15)
            Face.Instance.GetComponentInChildren<SpriteRenderer>().sprite = Face.Instance.listSprites[0];

        else if (GameManager.Instance.Score < 30 && GameManager.Instance.Score >= 15)
            Face.Instance.GetComponentInChildren<SpriteRenderer>().sprite = Face.Instance.listSprites[1];

        else
            Face.Instance.GetComponentInChildren<SpriteRenderer>().sprite = Face.Instance.listSprites[2];
    }

    private void OnSwipeRight()
    {

        GameManager.Instance.AddScore();
        source.PlayOneShot(slapSounds[Random.Range(0, 3)]);

        if (GameManager.Instance.Score < 15)
            Face.Instance.GetComponentInChildren<SpriteRenderer>().sprite = Face.Instance.listSprites[3];

        else if (GameManager.Instance.Score < 30 && GameManager.Instance.Score >= 15)
            Face.Instance.GetComponentInChildren<SpriteRenderer>().sprite = Face.Instance.listSprites[4];

        else
            Face.Instance.GetComponentInChildren<SpriteRenderer>().sprite = Face.Instance.listSprites[5];
    }

}