﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Timer : MonoBehaviour {

    Text TimerText;
    // Use this for initialization
    void Start()
    {
        TimerText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        TimerText.text = String.Format("{0:00} : {1:00}", GameManager.Instance.seconds, GameManager.Instance.miliseconds);

        if (GameManager.Instance.seconds < 10)
            TimerText.color = Color.red;
        else
            TimerText.color = Color.black;
    }
}
